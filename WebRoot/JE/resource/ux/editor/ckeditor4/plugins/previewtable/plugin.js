﻿ //自定义功能：预览功能
 CKEDITOR.plugins.add('previewtable', {
     init: function (editor) {
     	var a = {
	         exec: function (editor) {
	         	editor.ownerCt.previewForm(editor);
	         }
	     };
         editor.addCommand('previewtable', a);
         editor.ui.addButton('previewtable', {
             label: '预览',
             icon: this.path + 'preview.png',
             command: 'previewtable'
         });
     }
 });
