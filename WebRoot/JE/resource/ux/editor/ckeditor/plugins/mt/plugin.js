﻿ //自定义功能：选择模版
 CKEDITOR.plugins.add('mt', {
     init: function (editor) {
     	var a = {
	         exec: function (editor) {
				JE.selectWin({
					config : {
						xtype : 'gridssfield',
						configInfo : 'PC_SYS_HTMLMB,,,S',//字典或查询选择的配置信息
						fieldLabel : '富文本模版',
						whereSql : " and HTMLMB_QY = '1'"//启用
					},
					callback : function(field,recs){//回调函数 field没用，recs选中的信息
						var rec = recs[0];
						if(rec){
							var val = rec.get('HTMLMB_MB');
							val = decodeURIComponent(val);
							editor.setData(val);
						}
					} 
				});
	         }
	     };
         editor.addCommand('mt', a);
         editor.ui.addButton('mt', {
             label: '添加模版',
             icon: this.path + 'mt.png',
             command: 'mt'
         });
     }
 });
